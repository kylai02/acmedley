import request from "supertest";
import sinon from "sinon";
import * as dotenv from "dotenv";
import jwt, { type Secret } from "jsonwebtoken";
import app from "../src/app";
import * as activityModel from "../src/server/models/activity_model";
import * as userModel from "../src/server/models/user_model";

dotenv.config();
const { API_VERSION, MY_SECRET_KEY } = process.env;

const VALID_ID = "2",
  INVALID_ID = "-1";
const fakeActivity = {
  ID: 2,
  ANAME: "Act2",
  A_STIME: "2020-12-10T14:00:50.000Z",
  A_ETIME: "2020-12-20T14:00:50.000Z",
  R_STIME: "2020-11-10T14:00:50.000Z",
  R_ETIME: "2020-11-20T14:00:50.000Z",
  DESCRIPT: "test1",
  LOCAT: "nccu",
  MGR_UNAME: "李承恩"
};
const fakeParticipants = [
  {
    UEMAIL: "Andy@gmail.com",
    ID: 2,
    UNAME: "李承恩",
    RNAME: "test1",
    UPHONE: "123456"
  },
  {
    UEMAIL: "Andy2@gmail.com",
    ID: 2,
    UNAME: "承恩李",
    RNAME: "test2",
    UPHONE: "654321"
  }
];
const fakeUser = {
  user_name: "kyLai",
  user_mail: "kyLai@nccu.edu.tw"
};
const fakeUser2 = {
  user_name: "owenWang",
  user_mail: "owenWang@nccu.edu.tw"
};
const fakeHots = [
  {
    "ID": 5,
    "ANAME": "Act4",
    "A_STIME": "2020-12-10T14:00:50.000Z",
    "A_ETIME": "2020-12-20T14:00:50.000Z",
    "R_STIME": "2020-11-10T14:00:50.000Z",
    "R_ETIME": "2020-11-20T14:00:50.000Z",
    "DESCRIPT": "test2",
    "LOCAT": "nccu",
    "MGR_UEMAIL": "123456@gmail.com"
  },
  {
    "ID": 2,
    "ANAME": "Act2",
    "A_STIME": "2020-12-10T14:00:50.000Z",
    "A_ETIME": "2020-12-20T14:00:50.000Z",
    "R_STIME": "2020-11-10T14:00:50.000Z",
    "R_ETIME": "2020-11-20T14:00:50.000Z",
    "DESCRIPT": "test1",
    "LOCAT": "nccu",
    "MGR_UEMAIL": "Andy@gmail.com"
  },
  {
    "ID": 1,
    "ANAME": "Act1",
    "A_STIME": "2020-12-10T14:00:50.000Z",
    "A_ETIME": "2020-12-20T14:00:50.000Z",
    "R_STIME": "2020-11-10T14:00:50.000Z",
    "R_ETIME": "2020-11-20T14:00:50.000Z",
    "DESCRIPT": "test",
    "LOCAT": "nccu",
    "MGR_UEMAIL": "109703010@g.nccu.edu.tw"
  }
];

const fakeGetActivityById = async (activityId: String) => {
  if (activityId == VALID_ID) {
    return [fakeActivity];
  }
  if (activityId == INVALID_ID) {
    return "";
  }
};
const fakeGetParticipantsById = async (activityId: String) => {
  if (activityId == VALID_ID) {
    return fakeParticipants;
  }
  if (activityId == INVALID_ID) {
    return [];
  }
};
const fakeGetTokenByEmail = async (name: string, email: string) => {
  const payload = {
    user_name: name,
    user_mail: email
  };
  const SECRET_KEY: Secret = MY_SECRET_KEY ?? "mysecret";
  const token = jwt.sign(payload, String(SECRET_KEY), {
    expiresIn: "1 day"
  });
  return token;
};
const fakeVerifyParticipantsRead = async (
  activityId: string,
  email: string
) => {
  if (email === fakeUser.user_mail) {
    return true;
  }
  return false;
}
const fakeCreateRegistById = async (
  authoEmail: string,
  activityId: string,
  userInfo: any
) => {} // Do nothing
const fakeUpdateRegistById = async (
  authoEmail: string,
  activityId: string,
  userInfo: any
) => {} // Do nothing
const fakeVerifyRegistrationUpdate = async (
  activityId: string,
  email: string
) => {
  if (email === fakeUser.user_mail) {
    return true;
  }
  return false;
}
const fakeGetHotActivities = async () => {
  return fakeHots;
}

const activityStub = sinon
  .stub(activityModel, "getActivityById")
  .callsFake(fakeGetActivityById);
const participantsStub = sinon
  .stub(activityModel, "getParticipantsById")
  .callsFake(fakeGetParticipantsById);
const userStub = sinon
  .stub(userModel, "getTokenByEmail")
  .callsFake(fakeGetTokenByEmail);
const verifyParticipantsStub = sinon
  .stub(activityModel, "verifyParticipantsRead")
  .callsFake(fakeVerifyParticipantsRead);
const createRegisterStub = sinon
  .stub(activityModel, "createRegistById")
  .callsFake(fakeCreateRegistById);
const updateRegisterStub = sinon
  .stub(activityModel, "updateRegistById")
  .callsFake(fakeUpdateRegistById);
const verifyRegisterStub = sinon
  .stub(activityModel, "verifyRegistrationUpdate")
  .callsFake(fakeVerifyRegistrationUpdate);
const getHotsStub = sinon
  .stub(activityModel, "getHotActivities")
  .callsFake(fakeGetHotActivities);

describe("Test activity information", () => {
  test("Get activity Information with correct id", async () => {
    const res = await request(app)
      .get(`/api/${API_VERSION}/activity`)
      .query({ activityId: VALID_ID });
    expect(res.status).toEqual(200);
    expect(res.body).toEqual(fakeActivity);
  });

  test("Get activity Information with wrong id", async () => {
    const res = await request(app)
      .get(`/api/${API_VERSION}/activity`)
      .query({ activityId: INVALID_ID });
    expect(res.status).toEqual(200);
    expect(res.body).toEqual("");
  });

  test("Get activity Information without id", async () => {
    const res = await request(app).get(`/api/${API_VERSION}/activity`);
    expect(res.status).toEqual(400);
    expect(res.body).toEqual({ error: "Missing parameters" });
  });

  afterAll(() => {
    activityStub.restore();
  });
});

describe("Test participants of an activity", () => {
  let token: string, token2: string;
  beforeAll(async () => {
    const identity = await request(app)
      .post(`/account/login`)
      .send(fakeUser);
    token = identity.body.token;
    const identity2 = await request(app)
      .post(`/account/login`)
      .send(fakeUser2);
    token2 = identity2.body.token;
  });

  test("Get participants without login", async () => {
    const ERR_MSG = {"error": "Login first!"};
    const res = await request(app)
      .get(`/api/${API_VERSION}/activity/participants`)
      .query({ activityId: VALID_ID });
    expect(res.status).toEqual(401);
    expect(res.body).toEqual(ERR_MSG);
  });

  test("Get participants without permission", async () => {
    const res = await request(app)
      .get(`/api/${API_VERSION}/activity/participants`)
      .set("Authorization", `Bearer ${token2}`)
      .query({ activityId: VALID_ID });
    expect(res.status).toEqual(403);
    expect(res.body).toEqual({ error: "Unauthorized request" });
  });

  test("Get participants with correct id", async () => {
    const res = await request(app)
      .get(`/api/${API_VERSION}/activity/participants`)
      .set("Authorization", `Bearer ${token}`)
      .query({ activityId: VALID_ID });
    expect(res.status).toEqual(200);
    expect(res.body).toEqual(fakeParticipants);
  });

  test("Get participants with wrong id", async () => {
    const res = await request(app)
      .get(`/api/${API_VERSION}/activity/participants`)
      .set("Authorization", `Bearer ${token}`)
      .query({ activityId: INVALID_ID });
    expect(res.status).toEqual(200);
    expect(res.body).toEqual([]);
  });

  test("Get participants without id", async () => {
    const res = await request(app)
      .get(`/api/${API_VERSION}/activity/participants`)
      .set("Authorization", `Bearer ${token}`);
    expect(res.status).toEqual(400);
    expect(res.body).toEqual({ error: "Missing parameters" });
  });

  afterAll(() => {
    participantsStub.restore();
    verifyParticipantsStub.restore();
  });
});

describe("Test activity registration function", () => {
  let token: string, token2: string;
  beforeAll(async () => {
    const identity = await request(app)
      .post(`/account/login`)
      .send(fakeUser);
    token = identity.body.token;
    const identity2 = await request(app)
      .post(`/account/login`)
      .send(fakeUser2);
    token2 = identity2.body.token;
  });

  test("Create registration", async () => {
    const registrationData = { name: "kyLai", phone: "0987878787" }
    const res = await request(app)
      .post(`/api/${API_VERSION}/activity/register/create`)
      .query({ activityId: VALID_ID })
      .set("Authorization", `Bearer ${token}`)
      .send(registrationData);
    expect(res.status).toEqual(200);
    expect(res.body).toEqual("Successful");
  });

  test("Update other's registration", async () => {
    const registrationData = { name: "kyLai", phone: "0987878787" };
    const res = await request(app)
      .post(`/api/${API_VERSION}/activity/register/update`)
      .query({ activityId: VALID_ID })
      .set("Authorization", `Bearer ${token2}`)
      .send(registrationData);
    expect(res.status).toEqual(403);
    expect(res.body).toEqual({ error: "Unauthorized request" });
  });

  test("Update self registration", async () => {
    const registrationData = { name: "kyLai", phone: "0987878787" };
    const res = await request(app)
      .post(`/api/${API_VERSION}/activity/register/update`)
      .query({ activityId: VALID_ID })
      .set("Authorization", `Bearer ${token}`)
      .send(registrationData);
    expect(res.status).toEqual(200);
    expect(res.body).toEqual("Successful");
  });

  afterAll(() => {
    userStub.restore();
    createRegisterStub.restore();
    updateRegisterStub.restore();
    verifyRegisterStub.restore();
  });
});

describe("Test get hot activities", () => {
  test("Update self registration", async () => {
    const res = await request(app)
      .get(`/api/${API_VERSION}/activity/hots`);
    expect(res.status).toEqual(200);
    expect(res.body).toEqual(fakeHots);
  });
  afterAll(() => {
    getHotsStub.restore();
  });
});
