import request from "supertest";
import app from "../src/app";

describe("Test app.ts", () => {
  test("Get root route", async () => {
    const res = await request(app).get("/");
    expect(res.body).toEqual({ message: "Hello Express + TS!!~~" });
  });

  test("Get non-existent route", async () => {
    const res = await request(app).get("/non-exsitent-path");
    expect(res.body).toEqual({ error: "Invalid Path" });
  });
});
