import request from "supertest";
import sinon from "sinon";
import app from "../src/app";
import * as userModel from "../src/server/models/user_model";
import jwt, { Secret } from "jsonwebtoken";
import * as dotenv from "dotenv";

dotenv.config();
const { MY_SECRET_KEY } = process.env;
const SECRET_KEY: Secret = MY_SECRET_KEY ?? "mysecret";

const fakeGetTokenByEmail = async (
  name: string,
  email: string
): Promise<any> => {
  const payload = {
    user_name: name,
    user_mail: email
  };
  const token = jwt.sign(payload, SECRET_KEY, {
    expiresIn: "1 day"
  });
  return token;
};

const activityStub = sinon
  .stub(userModel, "getTokenByEmail")
  .callsFake(fakeGetTokenByEmail);

describe("Test login function", () => {
  test("Login without enough info", async () => {
    const res = await request(app).post(`/account/login`).send({});
    expect(res.status).toEqual(400);
    expect(res.body).toEqual({
      error: "Missing account info",
      msg: {}
    });
  });

  test("Login with enough info", async () => {
    const testPayload = {
      user_name: "owen",
      user_mail: "owen@ntu.com"
    };
    const res = await request(app).post(`/account/login`).send(testPayload);
    expect(res.status).toEqual(200);
    expect(res.body).toBeTruthy();
  });

  afterAll(() => {
    activityStub.restore();
  });
});
