import type { Request, Response } from "express";
import { type AuthoRequest } from "../../interface/interface";
import crypto from "crypto";
import {
  getActivityById,
  getParticipantsById,
  verifyParticipantsRead,
  createRegistById,
  updateRegistById,
  verifyRegistrationUpdate,
  getHotActivities,
  getHeldActivitiesByEmail,
  getRegisteredActivitiesByEmail,
  createActivityByEmail,
  updateActivityById
} from "../models/activity_model";

const getActivity = async (req: Request, res: Response): Promise<void> => {
  if (req.query.activityId === undefined) {
    res.status(400).send({
      error: "Missing parameters"
    });
    return;
  }
  const [activity] = await getActivityById(String(req.query.activityId));
  res.status(200).json(activity);
};

const getParticipants = async (req: Request, res: Response): Promise<void> => {
  if (req.query.activityId === undefined) {
    res.status(400).send({
      error: "Missing parameters"
    });
    return;
  }
  const authoEmail = (req as AuthoRequest).authoEmail;
  const activityId = String(req.query.activityId);
  if (!(await verifyParticipantsRead(activityId, authoEmail))) {
    res.status(403).send({
      error: "Unauthorized request"
    });
    return;
  }
  const participants = await getParticipantsById(activityId);
  res.status(200).json(participants);
};

const createRegistration = async (
  req: Request,
  res: Response
): Promise<void> => {
  if (req.query.activityId === undefined) {
    res.status(400).send({
      error: "Missing parameters"
    });
    return;
  } else if (req.body.name === undefined || req.body.phone === undefined) {
    res.status(400).send({
      error: "Missing personal info",
      msg: req.body
    });
    return;
  }
  const authoEmail = (req as AuthoRequest).authoEmail;
  const activityId = String(req.query.activityId);
  const userInfo = {
    name: req.body.name,
    phone: req.body.phone
  };
  await createRegistById(authoEmail, activityId, userInfo);
  res.status(200).json("Successful");
};

const updateRegistration = async (
  req: Request,
  res: Response
): Promise<void> => {
  if (req.query.activityId === undefined) {
    res.status(400).send({
      error: "Missing parameters"
    });
    return;
  } else if (req.body.name === undefined || req.body.phone === undefined) {
    res.status(400).send({
      error: "Missing personal info",
      msg: req.body
    });
    return;
  }
  const authoEmail = (req as AuthoRequest).authoEmail;
  const activityId = String(req.query.activityId);
  if (!(await verifyRegistrationUpdate(activityId, authoEmail))) {
    res.status(403).send({
      error: "Unauthorized request"
    });
    return;
  }
  const userInfo = {
    name: req.body.name,
    phone: req.body.phone
  };
  await updateRegistById(authoEmail, activityId, userInfo);
  res.status(200).json("Successful");
};

const getHots = async (req: Request, res: Response): Promise<void> => {
  const activities = await getHotActivities();
  res.status(200).json(activities);
};

const getHeldActivities = async (
  req: Request,
  res: Response
): Promise<void> => {
  const authoEmail = (req as AuthoRequest).authoEmail;
  const activities = await getHeldActivitiesByEmail(authoEmail);
  res.status(200).json(activities);
};

const getRegisteredActivities = async (
  req: Request,
  res: Response
): Promise<void> => {
  const authoEmail = (req as AuthoRequest).authoEmail;
  const activities = await getRegisteredActivitiesByEmail(authoEmail);
  res.status(200).json(activities);
};

const createActivity = async (req: Request, res: Response): Promise<void> => {
  const activityInfo = { ID: crypto.randomUUID(), ...req.body };
  const authoEmail = (req as AuthoRequest).authoEmail;
  const status = await createActivityByEmail(activityInfo, authoEmail);
  if (status) {
    res.status(200).send("Successful");
  } else {
    res.status(400).send({
      error: "Missing parameters"
    });
  }
};

const updateActivity = async (req: Request, res: Response): Promise<void> => {
  const activityInfo = req.body;
  const authoEmail = (req as AuthoRequest).authoEmail;
  const status = await updateActivityById(activityInfo, authoEmail);
  if (status) {
    res.status(200).send("Successful");
  } else {
    res.status(400).send({
      error: "Missing parameters"
    });
  }
};

export {
  getActivity,
  getParticipants,
  createRegistration,
  updateRegistration,
  getHots,
  getHeldActivities,
  getRegisteredActivities,
  createActivity,
  updateActivity
};
