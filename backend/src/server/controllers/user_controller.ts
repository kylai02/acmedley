import { getTokenByEmail } from "../models/user_model";
import type { Request, Response } from "express";

/* User  POST 登入(Login) */
const getToken = async (req: Request, res: Response): Promise<void> => {
  // 取得token
  // console.log(req.body);
  if (req.body.user_name === undefined || req.body.user_mail === undefined) {
    res.status(400).send({
      error: "Missing account info",
      msg: req.body
    });
    return;
  }
  const name = req.body.user_name;
  const email = req.body.user_mail;
  const token = await getTokenByEmail(name, email);
  // res.setHeader("Authorization", token);
  res.status(200).json({ name, email, token });
};

export { getToken };
