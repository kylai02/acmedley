import pool from "./mysqlcon";
import jwt, { type Secret } from "jsonwebtoken";
import * as dotenv from "dotenv";

dotenv.config();
const { MY_SECRET_KEY } = process.env;
const SECRET_KEY: Secret = MY_SECRET_KEY ?? "mysecret";

/*  User GET (Login)登入取得資訊  */
const getTokenByEmail = async (name: string, email: string): Promise<any> => {
  const [uname] = await pool.query(
    "SELECT `UNAME` FROM `USER` WHERE `UEMAIL` = ?",
    email
  );
  const dbUname = JSON.parse(JSON.stringify(uname))[0];

  // FIXME: database update protection not sufficient
  if (dbUname === undefined || dbUname.UNAME === name) {
    if (dbUname === undefined) {
      await pool.execute(
        "INSERT INTO `USER` (`UNAME`, `UEMAIL`) VALUES (?,?)",
        [name, email]
      );
    }
    const payload = {
      user_name: name,
      user_mail: email
    };
    const token = jwt.sign(payload, SECRET_KEY, {
      expiresIn: "1 day"
    });
    return token;
  } else {
    console.log(dbUname);
    throw new Error("Invalid user and email");
  }
};

export { getTokenByEmail };
