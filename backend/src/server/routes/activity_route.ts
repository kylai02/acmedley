import express from "express";
import { wrapAsync, ensureToken } from "../../util/util";
import {
  getActivity,
  getParticipants,
  createRegistration,
  updateRegistration,
  getHots,
  getHeldActivities,
  getRegisteredActivities,
  createActivity,
  updateActivity
} from "../controllers/activity_controller";

const router = express.Router();

router.route("/activity").get(wrapAsync(getActivity));
router
  .route("/activity/participants")
  .get(ensureToken, wrapAsync(getParticipants));
router
  .route("/activity/register/create")
  .post(ensureToken, wrapAsync(createRegistration));
router
  .route("/activity/register/update")
  .post(ensureToken, wrapAsync(updateRegistration));
router.route("/activity/hots").get(wrapAsync(getHots));
router.route("/activity/held").get(ensureToken, wrapAsync(getHeldActivities));
router
  .route("/activity/registered")
  .get(ensureToken, wrapAsync(getRegisteredActivities));
router.route("/activity/create").post(ensureToken, wrapAsync(createActivity));
router.route("/activity/update").post(ensureToken, wrapAsync(updateActivity));

export default router;
