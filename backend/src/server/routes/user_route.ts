import express from "express";
import { getToken } from "../controllers/user_controller";
import { wrapAsync } from "../../util/util";

const router = express.Router();

router.route("/login").post(wrapAsync(getToken));

export default router;
