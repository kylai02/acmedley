import type { Request, Response, NextFunction } from "express";
import jwt, { type Secret } from "jsonwebtoken";
import * as dotenv from "dotenv";
import { type AuthoRequest } from "../interface/interface";

dotenv.config();
const { MY_SECRET_KEY } = process.env;
const SECRET_KEY: Secret = MY_SECRET_KEY ?? "mysecret";

// reference: https://thecodebarbarian.com/80-20-guide-to-express-error-handling
const wrapAsync = (fn: any) => {
  return function (req: Request, res: Response, next: NextFunction) {
    // Make sure to `.catch()` any errors and pass them along to the `next()`
    // middleware in the chain, in this case the error handler.
    fn(req, res, next).catch(next);
  };
};

// reference: https://www.datainfinities.com/43/cant-set-headers-after-they-are-sent-to-the-client
// reference: https://github.com/andy6804tw/RESTful_API_start_kit/blob/master/src/server/routes/article.route.js
// reference: https://dev.to/juliecherner/authentication-with-jwt-tokens-in-typescript-with-express-3gb1
const ensureToken = (req: Request, res: Response, next: NextFunction): void => {
  const bearerToken = req.headers.authorization?.replace("Bearer ", "");
  if (bearerToken === undefined || bearerToken === "Bearer") {
    res.status(401).send({
      error: "Login first!"
    });
  } else {
    const tokenPayload = JSON.parse(
      JSON.stringify(jwt.verify(bearerToken, SECRET_KEY))
    );
    (req as AuthoRequest).authoEmail = tokenPayload.user_mail;
    next();
  }
};

export { wrapAsync, ensureToken };
