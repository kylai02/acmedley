import React from "react";
import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import ActivityDetailPage from "./pages/ActivityDetailPage";
import ActivityOwnerPage from "./pages/ActivityOwnerPage";
import ActivityRegisterPage from "./pages/ActivityRegisterPage";
import NewActivityPage from "./pages/NewActivityPage";
import ParticipantPanel from "./pages/ParticipantPanel";
import OwnerPanel from "./pages/OwnerPanel";

function App(): JSX.Element {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/activity/:activityId" element={<ActivityDetailPage />} />
      <Route
        path="/activity/:activityId/owner"
        element={<ActivityOwnerPage />}
      />
      <Route
        path="/activity/:activityId/register"
        element={<ActivityRegisterPage />}
      />
      <Route path="/participant" element={<ParticipantPanel />} />
      <Route path="/owner" element={<OwnerPanel />} />
      <Route path="/owner/new_activity" element={<NewActivityPage />} />
    </Routes>
  );
}

export default App;
