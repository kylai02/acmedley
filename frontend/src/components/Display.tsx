import React, { useRef, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import api from "../utils/api";

interface Activity {
  ID: number;
  ANAME: string;
  COVER: string;
}

const Display = (): JSX.Element => {
  const [activities, setActivities] = useState<Activity[]>([]);
  const displayRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    api
      .HomepageInfo()
      .then((res) => {
        setActivities(res);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  return (
    <div ref={displayRef} className="bg-background py-20 pl-36 pr-0.5">
      <div className="grid grid-flow-row grid-cols-3 gap-x-12 gap-y-8 font-noto_sans w-11/12">
        {activities.map((activity) => (
          <Link to={`/activity/${activity.ID}`} key={activity.ID}>
            <div className="flex h-72 items-center justify-center">
              <img
                src={activity.COVER}
                alt="activity_cover"
                className="h-full w-full rounded-xl"
              />
            </div>
            <div className="flex items-center justify-center pt-2 text-xl">
              {activity.ANAME}
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default Display;
