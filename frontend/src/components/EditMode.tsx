import React, { useState } from "react";
import Header from "./Header";

interface Activity {
  ID: number;
  ANAME: string;
  A_STIME: string;
  A_ETIME: string;
  R_STIME: string;
  R_ETIME: string;
  DESCRIPT: string;
  LOCAT: string;
  MGR_UEMAIL: string;
  IMAGES: string;
}

interface actProp {
  activity: Activity;
  toggleEdit: any;
}

export default function EditMode({
  activity,
  toggleEdit
}: actProp): JSX.Element {
  const [A_STIME, setAST] = useState(activity.A_STIME.slice(0, 16));
  const [A_ETIME, setAET] = useState(activity.A_STIME.slice(0, 16));
  const [R_STIME, setRST] = useState(activity.R_STIME.slice(0, 16));
  const [R_ETIME, setRET] = useState(activity.R_ETIME.slice(0, 16));
  const [location, setLocation] = useState(activity.LOCAT);
  const [description, setDescription] = useState();

  const handleDescription: any = (event: any) => {
    setDescription(event.target.value);
  };

  const Submit: any = (event: any) => {
    event.preventDefault();
  };

  return (
    <div>
      <Header />
      <div
        className="min-h-screen bg-background px-20 py-10 font-noto_sans"
        key={activity.ID}
      >
        <div className="pl-16 text-2xl">{activity.ANAME}</div>
        <hr className="my-7 ml-16 w-11/12 border-2 border-t border-gray" />
        <div className="relative me-12 ms-16">
          <img
            src={activity.IMAGES}
            alt="Activity Image"
            className="my-7 h-132 w-full rounded-3xl"
          />
          <div
            className="absolute inset-0 flex w-full items-center justify-center rounded-3xl bg-black bg-opacity-50"
            onClick={undefined}
          >
            <p className="text-xl text-white">編輯圖片</p>
          </div>
        </div>
        <div className="mb-96 me-12 ms-16 flex justify-center rounded-xl bg-nav">
          <div className="h-auto w-full rounded-md p-5 font-noto_sans">
            <div className="flex justify-between">
              <div>
                <h1 className="pb-2 text-xl">&#128205;日期及地點</h1>
                <div className="pr-auto block rounded-lg bg-white pb-8 pr-24">
                  <form onSubmit={Submit}>
                    <span className="mt-3 px-5 text-lg">日期:</span>
                    <input
                      type="datetime-local"
                      alt="AST"
                      value={A_STIME}
                      onChange={(e) => {
                        setAST(e.target.value);
                      }}
                    />
                    <span> ~ </span>
                    <input
                      type="datetime-local"
                      alt="AET"
                      value={A_ETIME}
                      onChange={(e) => {
                        setAET(e.target.value);
                      }}
                    />
                    <br />
                    <span className="px-5 text-lg">地點:</span>
                    <input
                      type="text"
                      value={location}
                      onChange={(e) => {
                        setLocation(e.target.value);
                      }}
                    />
                    <br />
                    <span className="px-5 text-lg">報名時間:&emsp;</span>
                    <input
                      type="datetime-local"
                      alt="RST"
                      value={R_STIME}
                      onChange={(e) => {
                        setRST(e.target.value);
                      }}
                    />
                    <span> ~ </span>
                    <input
                      type="datetime-local"
                      alt="RET"
                      value={R_ETIME}
                      onChange={(e) => {
                        setRET(e.target.value);
                      }}
                    />
                  </form>
                </div>
              </div>
              <div className="space-x-2">
                <button
                  className="rounded-lg bg-amber-300 px-4 py-1 font-bold text-white hover:bg-yellow-600"
                  onClick={toggleEdit}
                >
                  完成
                </button>
                <button
                  className="rounded-lg border-4 border-yellow-300 px-3 text-yellow-300"
                  style={{ backgroundColor: "#fffef0" }}
                  onClick={toggleEdit}
                >
                  取消
                </button>
              </div>
            </div>
            <hr className="my-5 flex w-7/12 border-2 border-t border-gray" />
            <h1 className="pb-2 text-xl">&#128205;活動資訊</h1>
            <div className="me-8 w-7/12 rounded-lg bg-white">
              <textarea
                className="w-full rounded-lg pl-2"
                rows={20}
                value={description}
                onChange={handleDescription}
              >
                {activity.DESCRIPT}
              </textarea>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
