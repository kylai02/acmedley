import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import "@testing-library/jest-dom";
import Header from "./Header";

describe("Header", () => {
  it("should toggle menuOpen state when handleMenuClick is called", () => {
    const { getByTestId, getAllByTestId } = render(
      <MemoryRouter basename="">
        <Header />
      </MemoryRouter>
    );
    const menuButton = getByTestId("menu-button");
    const hamburgerMenuLines = getAllByTestId("hamburger-menu-line");

    fireEvent.mouseEnter(menuButton);
    expect(menuButton).toHaveClass("hover:bg-nav");
    expect(menuButton).not.toHaveClass("bg-header");

    fireEvent.mouseLeave(menuButton);

    fireEvent.click(menuButton);
    expect(menuButton).toHaveClass("bg-nav");
    expect(menuButton).not.toHaveClass("bg-header");
    hamburgerMenuLines.forEach((line) => {
      expect(line).toHaveClass("bg-header");
    });

    fireEvent.click(menuButton);
    expect(menuButton).not.toHaveClass("bg-nav");
    hamburgerMenuLines.forEach((line) => {
      expect(line).not.toHaveClass("bg-header");
    });
  });

  it("should render menu options when menuOpen is true", () => {
    const { getByTestId } = render(
      <MemoryRouter basename="">
        <Header />
      </MemoryRouter>
    );
    const menuButton = getByTestId("menu-button");

    fireEvent.click(menuButton);
    const mainMenu = getByTestId("nav-menu");
    expect(mainMenu).toBeInTheDocument();

    const menuOptionOne = getByTestId("menu-first-option");
    fireEvent.mouseEnter(menuOptionOne);
    expect(menuOptionOne).toHaveClass("hover:bg-gray_limpid");

    const menuOptionTwo = getByTestId("menu-second-option");
    fireEvent.mouseEnter(menuOptionTwo);
    expect(menuOptionTwo).toHaveClass("hover:bg-gray_limpid");
  });

  it("should navigate to home page when logo is clicked", () => {
    render(
      <MemoryRouter>
        <Header />
      </MemoryRouter>
    );

    const logoLink = screen.getByAltText("Logo");
    fireEvent.click(logoLink);

    // 斷言是否導航到首頁
    expect(window.location.pathname).toBe("/");
  });
});
