import React, { useState, useEffect } from "react";
import api from "../utils/api";
import { useParams, Link } from "react-router-dom";
import timesplit from "../utils/TimeSplit";
import Header from "../components/Header";

interface Activity {
  ID: number;
  ANAME: string;
  A_STIME: string;
  A_ETIME: string;
  R_STIME: string;
  R_ETIME: string;
  DESCRIPT: string;
  LOCAT: string;
  MGR_UEMAIL: string;
  IMAGES: string;
}

export default function ActivityDetailPage(): JSX.Element {
  const id = useParams().activityId;
  const [activity, setActivities] = useState<Activity>();

  useEffect(() => {
    api
      .ActivityList(String(id))
      .then((res) => {
        const activity: Activity = {
          ID: res.ID,
          ANAME: res.ANAME,
          A_STIME: res.A_STIME,
          A_ETIME: res.A_ETIME,
          R_STIME: res.R_STIME,
          R_ETIME: res.R_ETIME,
          DESCRIPT: res.DESCRIPT,
          LOCAT: res.LOCAT,
          MGR_UEMAIL: res.MGR_UEMAIL,
          IMAGES: res.IMAGES
        };
        setActivities(activity);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [id]);
  if (activity === undefined) {
    return <div>Wrong</div>;
  }
  const link = `/activity/${String(id)}/register`;
  let ADate = null;
  const [ASDate, ASTime] = timesplit(activity.A_STIME);
  const [AEDate, AETime] = timesplit(activity.A_ETIME);
  if (ASDate === AEDate) {
    ADate = ASDate + " " + ASTime;
  } else {
    ADate = ASDate + " (" + ASTime + ") ~ " + AEDate + " (" + AETime + ")";
  }
  let RDate = null;
  const [RSDate, RSTime] = timesplit(activity.R_STIME);
  const [REDate, RETime] = timesplit(activity.R_ETIME);
  RDate = RSDate + " (" + RSTime + ") ~ " + REDate + " (" + RETime + ")";
  return (
    <div>
      <Header />
      <div key={activity.ID} className="min-h-screen bg-background px-20 py-10">
        <div className="pl-16 font-noto_sans text-2xl">{activity.ANAME}</div>
        <hr className="my-7 ml-16 w-11/12 border-2 border-t border-gray" />
        <div className="me-12 ms-16 flex justify-center">
          <img
            src={activity.IMAGES}
            alt="Activity Image"
            className="mb-7 h-132 w-full rounded-3xl"
          />
        </div>
        <div className="mb-96 me-12 ms-14 flex justify-center rounded-xl bg-nav">
          <div className="h-auto w-full rounded-md p-5 font-noto_sans">
            <div className="flex justify-between">
              <div>
                <h1 className="text-xl">&#128205;日期及地點</h1>
                <p className="mt-3 px-5 text-lg">日期:&emsp;{ADate}</p>
                <p className="px-5 text-lg">地點:&emsp;{activity.LOCAT}</p>
                <p className="px-5 text-lg">報名時間:&emsp;{RDate}</p>
              </div>
              <div className="mr-8 w-56 rounded-xl border-2 border-amber-300 bg-background">
                <div>
                  <p className="flex justify-center pt-2 text-gray">
                    {activity.ANAME}
                  </p>
                  <p className="mr-10 flex justify-center py-2 ps-10 text-gray">
                    {ADate}
                  </p>
                </div>
                <div className="flex justify-center pb-4">
                  <Link to={link}>
                    <button className="rounded-lg bg-amber-300 px-8 py-2 font-noto_sans text-white hover:bg-yellow-600">
                      我要報名
                    </button>
                  </Link>
                </div>
              </div>
            </div>
            <hr className="mb-5 flex w-3/4 border-2 border-t border-gray" />
            <h1 className="text-xl">&#128205;活動資訊</h1>
            <p className="mt-3 px-5 text-lg">簡介:&emsp;{activity.DESCRIPT}</p>
          </div>
        </div>
      </div>
    </div>
  );
}
