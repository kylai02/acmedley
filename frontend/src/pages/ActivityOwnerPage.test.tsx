import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter, Route, Routes } from "react-router-dom";
import ActivityDetailPage from "../pages/ActivityDetailPage";
import "@testing-library/jest-dom/extend-expect"; // Import the extend-expect utility
import ActivityOwnerPage from "./ActivityOwnerPage";
import { act } from "react-dom/test-utils";

jest.mock("node-fetch");

describe("ActivityDetailPage", () => {
  beforeEach(() => {
    jest.resetModules();
  });

  it("renders activity details", async () => {
    // Mock the fetch response
    const mockResponse = {
      ID: 2,
      ANAME: "Act2",
      A_STIME: "2020-12-10T14:00:50.000Z",
      A_ETIME: "2020-12-20T14:00:50.000Z",
      R_STIME: "2020-11-10T14:00:50.000Z",
      R_ETIME: "2020-11-20T14:00:50.000Z",
      DESCRIPT: "test1",
      LOCAT: "nccu"
    };
    const mockParticipantListResponse = [
      {
        ID: 1,
        UPHONE: "123456789",
        UEMAIL: "participant1@example.com",
        UNAME: "Participant 1",
        RNAME: "Role 1"
      },
      {
        ID: 2,
        UPHONE: "987654321",
        UEMAIL: "participant2@example.com",
        UNAME: "Participant 2",
        RNAME: "Role 2"
      }
    ];
    global.fetch = jest
      .fn()
      .mockResolvedValueOnce({
        json: jest.fn().mockResolvedValue(mockResponse)
      })
      .mockResolvedValueOnce({
        json: jest.fn().mockResolvedValue(mockParticipantListResponse)
      }) as any;

    render(
      <MemoryRouter initialEntries={["/activity/2"]}>
        <Routes>
          <Route
            path="/activity/:activityId"
            element={<ActivityDetailPage />}
          />
        </Routes>
      </MemoryRouter>
    );

    await screen.findAllByRole("img");
    const activityPicture = screen.getAllByRole("img");
    expect(activityPicture).toHaveLength(2);
    const memberIdElement = screen.getByText((content, element) => {
      return content.includes("nccu");
    });
    expect(memberIdElement).toBeInTheDocument();
    const activityPictureComponents = screen.getAllByText("Act2");
    expect(activityPictureComponents).toHaveLength(2);
  });

  it("toggle edit mode", async () => {
    // Mock the fetch response
    const mockResponse = {
      ID: 2,
      ANAME: "Act2",
      A_STIME: "2020-12-10T14:00:50.000Z",
      A_ETIME: "2020-12-20T14:00:50.000Z",
      R_STIME: "2020-11-10T14:00:50.000Z",
      R_ETIME: "2020-11-20T14:00:50.000Z",
      DESCRIPT: "test1",
      LOCAT: "nccu",
      MGR_UNAME: "李承恩"
    };
    const mockParticipantListResponse = [
      {
        ID: 1,
        UPHONE: "123456789",
        UEMAIL: "participant1@example.com",
        UNAME: "Participant 1",
        RNAME: "Role 1"
      },
      {
        ID: 2,
        UPHONE: "987654321",
        UEMAIL: "participant2@example.com",
        UNAME: "Participant 2",
        RNAME: "Role 2"
      }
    ];
    global.fetch = jest
      .fn()
      .mockResolvedValueOnce({
        json: jest.fn().mockResolvedValue(mockResponse)
      })
      .mockResolvedValueOnce({
        json: jest.fn().mockResolvedValue(mockParticipantListResponse)
      }) as any;

    render(
      <MemoryRouter initialEntries={["/activity/2/owner"]}>
        <Routes>
          <Route
            path="/activity/:activityId/owner"
            element={<ActivityOwnerPage />}
          />
        </Routes>
      </MemoryRouter>
    );

    await screen.findAllByRole("button");
    let buttons = screen.getAllByRole("button");
    let editButton = buttons[0];
    // Initially, edit mode should be false
    expect(editButton).toBeInTheDocument();
    expect(editButton).toHaveTextContent("編輯");

    // Click the edit button inside act()
    act(() => {
      fireEvent.click(editButton);
    });
    // After clicking, edit mode should be true
    buttons = screen.getAllByRole("button");
    expect(buttons[0]).toHaveTextContent("完成");
    expect(buttons[1]).toHaveTextContent("取消");

    // Click the edit button again inside act()
    act(() => {
      fireEvent.click(buttons[1]);
    });

    // After clicking again, edit mode should be false
    buttons = screen.getAllByRole("button");
    editButton = buttons[0];
    expect(editButton).toHaveTextContent("編輯");
  });

  it("switch to edit mode and editting is working", async () => {
    // Mock the fetch response
    const mockResponse = {
      ID: 2,
      ANAME: "Act2",
      A_STIME: "2020-12-10T14:00:50.000Z",
      A_ETIME: "2020-12-20T14:00:50.000Z",
      R_STIME: "2020-11-10T14:00:50.000Z",
      R_ETIME: "2020-11-20T14:00:50.000Z",
      DESCRIPT: "test1",
      LOCAT: "nccu",
      MGR_UNAME: "李承恩"
    };
    const mockParticipantListResponse = [
      {
        ID: 1,
        UPHONE: "123456789",
        UEMAIL: "participant1@example.com",
        UNAME: "Participant 1",
        RNAME: "Role 1"
      },
      {
        ID: 2,
        UPHONE: "987654321",
        UEMAIL: "participant2@example.com",
        UNAME: "Participant 2",
        RNAME: "Role 2"
      }
    ];
    global.fetch = jest
      .fn()
      .mockResolvedValueOnce({
        json: jest.fn().mockResolvedValue(mockResponse)
      })
      .mockResolvedValueOnce({
        json: jest.fn().mockResolvedValue(mockParticipantListResponse)
      }) as any;

    render(
      <MemoryRouter initialEntries={["/activity/2/owner"]}>
        <Routes>
          <Route
            path="/activity/:activityId/owner"
            element={<ActivityOwnerPage />}
          />
        </Routes>
      </MemoryRouter>
    );
    await screen.findAllByRole("button");
    const button = screen.getByRole("button");
    // Click the edit button inside act()
    act(() => {
      fireEvent.click(button);
    });

    const location = screen.getByDisplayValue("nccu");
    act(() => {
      fireEvent.change(location, { target: { value: "somewhere" } });
    });
    expect(location).toHaveValue("somewhere");

    const descriptionTextarea = screen.getByText("test1");
    act(() => {
      fireEvent.change(descriptionTextarea, {
        target: { value: "New description" }
      });
    });
    expect(descriptionTextarea).toHaveValue("New description");
    // times
    const AST = screen.getByAltText("AST");
    fireEvent.change(AST, { target: { value: "2023-06-06T15:45" } });
    expect(AST).toHaveValue("2023-06-06T15:45");

    const AET = screen.getByAltText("AET");
    fireEvent.change(AET, { target: { value: "2023-06-06T15:48" } });
    expect(AET).toHaveValue("2023-06-06T15:48");

    const RST = screen.getByAltText("RST");
    fireEvent.change(RST, { target: { value: "2023-06-06T15:50" } });
    expect(RST).toHaveValue("2023-06-06T15:50");

    const RET = screen.getByAltText("RET");
    fireEvent.change(RET, { target: { value: "2023-06-06T20:44" } });
    expect(RET).toHaveValue("2023-06-06T20:44");
  });
});
