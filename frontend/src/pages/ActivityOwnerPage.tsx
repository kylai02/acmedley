import React, { useState, useEffect } from "react";
import api from "../utils/api";
import { useParams } from "react-router-dom";
import timesplit from "../utils/TimeSplit";
import EditMode from "../components/EditMode";
import Header from "../components/Header";

interface Activity {
  ID: number;
  ANAME: string;
  A_STIME: string;
  A_ETIME: string;
  R_STIME: string;
  R_ETIME: string;
  DESCRIPT: string;
  LOCAT: string;
  MGR_UEMAIL: string;
  IMAGES: string;
}

interface Participant {
  ID: number;
  RNAME: string;
  UEMAIL: string;
  UNAME: string;
  UPHONE: string;
}

export default function ActivityOwnerPage(): JSX.Element {
  const id = useParams().activityId;
  const [activity, setActivities] = useState<Activity>();
  const [participants, setParticipants] = useState<Participant[]>();
  useEffect(() => {
    api
      .ActivityList(String(id))
      .then((res) => {
        const activity: Activity = {
          ID: res.ID,
          ANAME: res.ANAME,
          A_STIME: res.A_STIME,
          A_ETIME: res.A_ETIME,
          R_STIME: res.R_STIME,
          R_ETIME: res.R_ETIME,
          DESCRIPT: res.DESCRIPT,
          LOCAT: res.LOCAT,
          MGR_UEMAIL: res.MGR_UEMAIL,
          IMAGES: res.IMAGES
        };
        setActivities(activity);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [id]);
  useEffect(() => {
    api
      .ParticipantList(String(id))
      .then((res) => {
        const participants = res.map((part) => ({
          ID: part.ID,
          UPHONE: part.UPHONE,
          UEMAIL: part.UEMAIL,
          UNAME: part.UNAME,
          RNAME: part.RNAME
        }));
        setParticipants(participants);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [id]);
  const [edit, setShow] = useState(false);
  const toggleEdit: any = () => {
    setShow(!edit);
  };

  if (activity === undefined || participants === undefined) {
    return <div>資料讀取or沒有管理權限</div>;
  }
  let ADate = null;
  const [ASDate, ASTime] = timesplit(activity.A_STIME);
  const [AEDate, AETime] = timesplit(activity.A_ETIME);
  if (ASDate === AEDate) {
    ADate = ASDate + " " + ASTime;
  } else {
    ADate = ASDate + " (" + ASTime + ") ~ " + AEDate + " (" + AETime + ")";
  }
  let RDate = null;
  const [RSDate, RSTime] = timesplit(activity.R_STIME);
  const [REDate, RETime] = timesplit(activity.R_ETIME);
  RDate = RSDate + " (" + RSTime + ") ~ " + REDate + " (" + RETime + ")";

  if (edit) {
    return <EditMode activity={activity} toggleEdit={toggleEdit} />;
  }

  return (
    <div>
      <Header />
      <div className="min-h-screen bg-background px-20 py-10" key={activity.ID}>
        <div className="pl-16 font-noto_sans text-2xl">{activity.ANAME}</div>
        <hr className="my-7 ml-16 w-11/12 border-2 border-t border-gray" />
        <div className="me-12 ms-16 flex justify-center">
          <img
            src={activity.IMAGES}
            alt="Activity Image"
            className="mb-7 h-132 w-full rounded-3xl"
          />
        </div>
        <div className="mb-96 me-12 ms-16 flex justify-center rounded-xl bg-nav">
          <div className="h-auto w-full rounded-md p-5 font-noto_sans">
            <div className="flex justify-between">
              <div>
                <h1 className="text-xl">&#128205;日期及地點</h1>
                <p className="mt-3 px-5 text-lg">日期:&emsp;{ADate}</p>
                <p className="px-5 text-lg">地點:&emsp;{activity.LOCAT}</p>
                <p className="px-5 text-lg">報名時間:&emsp;{RDate}</p>
              </div>
              <div>
                <button
                  className="mr-16 rounded-md border-4 border-yellow-300 bg-background p-2 text-yellow-300"
                  onClick={toggleEdit}
                >
                  &#128221;&ensp;編輯
                </button>
              </div>
            </div>
            <hr className="my-5 flex w-3/4 border-2 border-t border-gray" />
            <h1 className="text-xl">&#128205;活動資訊</h1>
            <p className="mt-3 px-5 text-lg">簡介:&emsp;{activity.DESCRIPT}</p>
            <hr className="my-5 flex w-3/4 border-2 border-t border-gray" />
            <h1 className="text-xl">&#128205;報名者表單</h1>
            <table className="mt-5 h-auto w-3/4 border-collapse bg-background text-left">
              <thead>
                <tr>
                  <th className="w-1/4 border-2 border-orange-300 px-6 py-3 font-noto_sans text-gray">
                    姓名
                  </th>
                  <th className="border-2 border-orange-300 px-6 py-3 font-noto_sans text-gray">
                    Email
                  </th>
                  <th className="font-noto_snas border-2 border-orange-300 px-6 py-3 text-gray">
                    連絡電話
                  </th>
                </tr>
              </thead>
              <tbody>
                {participants.map((participant) => (
                  <tr key={participant.UEMAIL}>
                    <td className="border-2 border-orange-300 px-6 py-4">
                      {participant.UNAME}
                    </td>
                    <td className="border-2 border-orange-300 px-6 py-4">
                      {participant.UEMAIL}
                    </td>
                    <td className="border-2 border-orange-300 px-6 py-4">
                      {participant.UPHONE}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}
