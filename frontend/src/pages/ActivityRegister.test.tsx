import React from "react";
import { render, screen } from "@testing-library/react";
import { MemoryRouter, Routes, Route } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import ActivityRegisterPage from "./ActivityRegisterPage";

jest.mock("node-fetch");
describe("ActivityRegisterPage", () => {
  beforeEach(() => {
    jest.resetModules();
  });
  it("test show input ", async () => {
    const mockResponse = {
      ID: 2,
      ANAME: "Act2",
      A_STIME: "2020-12-10T14:00:50.000Z",
      A_ETIME: "2020-12-20T14:00:50.000Z",
      R_STIME: "2020-11-10T14:00:50.000Z",
      R_ETIME: "2020-11-20T14:00:50.000Z",
      DESCRIPT: "test1",
      LOCAT: "nccu",
      MGR_UEMAIL: "Andy@gmail.com"
    };
    global.fetch = jest.fn().mockResolvedValue({
      json: jest.fn().mockResolvedValue(mockResponse)
    }) as any;
    render(
      <MemoryRouter initialEntries={["/activity/2/register"]}>
        <Routes>
          <Route
            path="/activity/:activityId/register"
            element={<ActivityRegisterPage />}
          />
        </Routes>
      </MemoryRouter>
    );
    await screen.findAllByRole("textbox");
    const inputNum = screen.getAllByRole("textbox");
    expect(inputNum).toHaveLength(2);
  });
});
