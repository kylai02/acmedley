import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect"; // Import the extend-expect utility
import NewActivityPage from "./NewActivityPage";

describe("NewActivityPage", () => {
  test("displays the initial state correctly", () => {
    render(
      <MemoryRouter>
        <NewActivityPage />
      </MemoryRouter>
    );

    const inputs = screen.getAllByRole("textbox");
    const titleInput = inputs[0];
    expect(titleInput).toHaveValue("新增活動標題");

    const locationInput = inputs[1];
    expect(locationInput).toHaveValue("新增地點");

    const descriptionTextArea = inputs[2];
    expect(descriptionTextArea).toHaveValue("新增活動資訊");
  });

  test("updates the state correctly when inputs change", () => {
    render(
      <MemoryRouter>
        <NewActivityPage />
      </MemoryRouter>
    );

    const inputs = screen.getAllByRole("textbox");
    const titleInput = inputs[0];
    fireEvent.change(titleInput, { target: { value: "New Title" } });
    expect(titleInput).toHaveValue("New Title");

    const locationInput = inputs[1];
    fireEvent.change(locationInput, { target: { value: "New Location" } });
    expect(locationInput).toHaveValue("New Location");

    const descriptionTextArea = inputs[2];
    fireEvent.change(descriptionTextArea, {
      target: { value: "New Description" }
    });
    expect(descriptionTextArea).toHaveValue("New Description");

    const AST = screen.getByAltText("AST");
    fireEvent.change(AST, { target: { value: "2023-06-06T15:45" } });
    expect(AST).toHaveValue("2023-06-06T15:45");

    const AET = screen.getByAltText("AET");
    fireEvent.change(AET, { target: { value: "2023-06-06T15:48" } });
    expect(AET).toHaveValue("2023-06-06T15:48");

    const RST = screen.getByAltText("RST");
    fireEvent.change(RST, { target: { value: "2023-06-06T15:50" } });
    expect(RST).toHaveValue("2023-06-06T15:50");

    const RET = screen.getByAltText("RET");
    fireEvent.change(RET, { target: { value: "2023-06-06T20:44" } });
    expect(RET).toHaveValue("2023-06-06T20:44");
  });

  test("image upload", () => {
    render(
      <MemoryRouter>
        <NewActivityPage />
      </MemoryRouter>
    );
    // open URL input
    let editButton = screen.getByText("上傳圖片");
    fireEvent.click(editButton);
    // change URL
    const URLInput = screen.getByAltText("imageURL");
    expect(URLInput).toBeInTheDocument();
    fireEvent.change(URLInput, {
      target: { value: "https://i.imgur.com/x5nh77H.png" }
    });
    expect(URLInput).toHaveValue("https://i.imgur.com/x5nh77H.png");
    // submit URL & close input
    const confirmButton = screen.getByText("確認");
    fireEvent.click(confirmButton);
    editButton = screen.getByText("更改圖片");
    expect(editButton).toBeInTheDocument();
    const image: any = screen.getByAltText("Activity Image");
    expect(image.src).toBe("https://i.imgur.com/x5nh77H.png");
  });
});
