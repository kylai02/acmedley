import React, { useState } from "react";
import Header from "../components/Header";
import { useNavigate, Link } from "react-router-dom";
import api from "../utils/api";

interface Activity {
  ANAME: string;
  A_STIME: string;
  A_ETIME: string;
  R_STIME: string;
  R_ETIME: string;
  DESCRIPT: string;
  LOCAT: string;
  MGR_UEMAIL: string;
  IMAGES: string;
}

export default function NewActivityPage(): JSX.Element {
  const [imageURL, setImageURL] = useState("");
  const [InputDisplay, setInputDisplay] = useState(false);
  const [URLInput, setURLInput] = useState("");
  const [title, setTitle] = useState("新增活動標題");
  const [A_STIME, setAST] = useState("");
  const [A_ETIME, setAET] = useState("");
  const [R_STIME, setRST] = useState("");
  const [R_ETIME, setRET] = useState("");
  const [location, setLocation] = useState("新增地點");
  const [description, setDescription] = useState("新增活動資訊");
  const navigate = useNavigate();
  const Submit: any = (event: any) => {
    event.preventDefault();
    if (A_STIME === "" || A_ETIME === "" || R_STIME === "" || R_ETIME === "") {
      alert("時間不得為空");
      return;
    }
    if (imageURL === "") {
      alert("圖片不得為空");
      return;
    }
    const activity: Activity = {
      ANAME: title,
      A_STIME,
      A_ETIME,
      R_STIME,
      R_ETIME,
      DESCRIPT: description,
      LOCAT: location,
      MGR_UEMAIL: "",
      IMAGES: imageURL
    };
    void api.createActivity(activity);
    navigate(-1);
  };

  return (
    <div>
      <Header />
      <div className="min-h-screen bg-background py-10 pl-40 pr-32 font-noto_sans">
        <input
          type="text"
          value={title}
          onChange={(e) => {
            setTitle(e.target.value);
          }}
          className="bg-background text-2xl"
        />
        <hr className="my-7 border-2 border-t border-gray" />
        <div className="relative">
          {imageURL === "" && (
            <span className="my-8 block h-132 w-full rounded-3xl" />
          )}
          {imageURL !== "" && (
            <img
              src={imageURL}
              alt="Activity Image"
              className="mb-7 h-132 w-full rounded-3xl"
            />
          )}
          <div className="absolute inset-0 flex items-center justify-center rounded-3xl bg-black bg-opacity-50">
            {!InputDisplay && (
              <label
                className="text-xl text-white"
                onClick={() => {
                  setInputDisplay(true);
                }}
              >
                {imageURL === "" ? "上傳圖片" : "更改圖片"}
              </label>
            )}
            {InputDisplay && (
              <div className="grid w-1/3 grid-cols-5 rounded-lg bg-nav px-4 py-2">
                <div className="col-span-4 pr-4">
                  <input
                    type="text"
                    alt="imageURL"
                    value={URLInput}
                    onChange={(e) => {
                      setURLInput(e.target.value);
                    }}
                    className="h-full w-full"
                  />
                </div>
                <button
                  className="col-span-1 rounded-lg bg-amber-300 px-4 py-1 font-bold text-white hover:bg-yellow-600"
                  onClick={() => {
                    setImageURL(URLInput);
                    setInputDisplay(false);
                  }}
                >
                  確認
                </button>
              </div>
            )}
          </div>
        </div>
        <div className="flex justify-center rounded-xl bg-nav">
          <div className="h-auto w-full rounded-md p-5">
            <div className="flex justify-between">
              <div>
                <h1 className="pb-2 text-xl">&#128205;日期及地點</h1>
                <div className="pr-auto block rounded-lg bg-white pr-24">
                  <form onSubmit={Submit}>
                    <span className="mt-3 px-5 text-lg">日期:</span>
                    <input
                      type="datetime-local"
                      alt="AST"
                      value={A_STIME}
                      onChange={(e) => {
                        setAST(e.target.value);
                      }}
                    />
                    <span> ~ </span>
                    <input
                      type="datetime-local"
                      alt="AET"
                      value={A_ETIME}
                      onChange={(e) => {
                        setAET(e.target.value);
                      }}
                    />
                    <br />
                    <span className="px-5 text-lg">地點:</span>
                    <input
                      type="text"
                      value={location}
                      onChange={(e) => {
                        setLocation(e.target.value);
                      }}
                    />
                    <br />
                    <span className="px-5 text-lg">報名時間:&emsp;</span>
                    <input
                      type="datetime-local"
                      alt="RST"
                      value={R_STIME}
                      onChange={(e) => {
                        setRST(e.target.value);
                      }}
                    />
                    <span> ~ </span>
                    <input
                      type="datetime-local"
                      alt="RET"
                      value={R_ETIME}
                      onChange={(e) => {
                        setRET(e.target.value);
                      }}
                    />
                  </form>
                </div>
              </div>
              <div className="space-x-2">
                <button
                  className="rounded-lg bg-amber-300 px-4 py-1 font-bold text-white hover:bg-yellow-600"
                  onClick={Submit}
                >
                  完成
                </button>
                <button
                  className="rounded-lg border-4 border-yellow-300 px-3 text-yellow-300"
                  style={{ backgroundColor: "#fffef0" }}
                >
                  <Link to={"/"}>取消</Link>
                </button>
              </div>
            </div>
            <hr className="my-5 flex w-7/12 border-2 border-t border-gray" />
            <h1 className="pb-2 text-xl">&#128205;活動資訊</h1>
            <div className="w-7/12 rounded-lg bg-white">
              <textarea
                className="w-full rounded-lg pl-2"
                rows={20}
                value={description}
                onChange={(e) => {
                  setDescription(e.target.value);
                }}
              >
                新增活動資訊
              </textarea>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
