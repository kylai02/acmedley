import React, { useEffect, useState } from "react";
import api from "../utils/api";
import Header from "../components/Header";
import { Link } from "react-router-dom";
import { FaPlusCircle } from "react-icons/fa";

interface ActivityInfo {
  ID: number;
  ANAME: string;
  IMAGE: string;
}
export default function OwnerPanel(): JSX.Element {
  const [activities, setActivities] = useState<ActivityInfo[]>();
  useEffect(() => {
    api
      .getHeldActivities()
      .then((res) => {
        const activity = res.map((Info: ActivityInfo) => ({
          ID: Info.ID,
          ANAME: Info.ANAME,
          IMAGE: Info.IMAGE
        }));
        setActivities(activity);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);
  if (activities === undefined) {
    return <div>Wait....</div>;
  }
  return (
    <div>
      <Header />
      <div className="min-h-screen bg-background px-20 py-10 font-noto_sans">
        <div className="flex items-center justify-between">
          <div className="pl-16 text-2xl">我舉辦的活動</div>
          <Link to="/owner/new_activity">
            <button className="flex items-center pr-12">
              <FaPlusCircle className="mr-1 h-6 w-6 text-yellow-500" />
              加入活動
            </button>
          </Link>
        </div>
        <hr className="my-7 ml-16 w-11/12 border-2 border-t border-gray" />
        <div className="grid grid-cols-3 gap-4">
          {activities.map((activity) => (
            <div key={activity.ID}>
              <Link to={`/activity/${activity.ID}/owner`}>
                <div className="flex h-72 items-center justify-center">
                  <img
                    src={activity.IMAGE}
                    alt="activity_image"
                    className="h-full w-full rounded-xl"
                  />
                </div>
              </Link>
              <div className="flex items-center justify-center pt-2 text-xl">
                {activity.ANAME}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
