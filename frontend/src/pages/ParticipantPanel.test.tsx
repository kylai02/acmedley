import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect"; // Import the extend-expect utility
import ParticipantPanel from "./ParticipantPanel";
import { MemoryRouter } from "react-router-dom";

jest.mock("node-fetch");

describe("ParticipantPanel Page", () => {
  beforeEach(() => {
    jest.resetModules();
  });

  it("renders ParticipantPanel activity details", async () => {
    // Mock the fetch response
    const mockResponse = [
      {
        ID: 2,
        ANAME: "Act2",
        IMAGE: "https://i.imgur.com/Yk3IJaq.jpg"
      }
    ];

    global.fetch = jest.fn().mockResolvedValueOnce({
      json: jest.fn().mockResolvedValue(mockResponse)
    });
    render(
      <MemoryRouter>
        <ParticipantPanel />
      </MemoryRouter>
    );

    await screen.findByText("Act2");
    const activityElement = screen.getByText("Act2");
    expect(activityElement).toBeInTheDocument();
  });
});
