import React, { useEffect, useState } from "react";
import api from "../utils/api";
import Header from "../components/Header";
import { Link } from "react-router-dom";
interface ActivityInfo {
  ID: number;
  ANAME: string;
  IMAGE: string;
}
export default function ParticipantPanel(): JSX.Element {
  const [activities, setActivities] = useState<ActivityInfo[]>();
  useEffect(() => {
    api
      .getRegisteredActivitie()
      .then((res) => {
        const activity = res.map((Info: ActivityInfo) => ({
          ID: Info.ID,
          ANAME: Info.ANAME,
          IMAGE: Info.IMAGE
        }));
        setActivities(activity);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);
  if (activities === undefined) {
    return <div>Wait....</div>;
  }
  return (
    <div>
      <Header />
      <div className="bg-background pl-40 pr-32 font-noto_sans">
        <div className="pt-9 text-2xl">我參加的活動</div>
        <hr className="my-6 border-2 border-t border-gray" />
        <div className="grid grid-flow-row grid-cols-3 gap-4 gap-x-12 gap-y-8 py-4">
          {activities.map((activity) => (
            <div key={activity.ID}>
              <Link to={`/activity/${activity.ID}`}>
                <div className="flex h-72 items-center justify-center">
                  <img
                    src={activity.IMAGE}
                    alt="activity_image"
                    className="h-full w-full rounded-xl"
                  />
                </div>
              </Link>
              <div className="flex items-center justify-center pt-2 text-xl">
                {activity.ANAME}
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
